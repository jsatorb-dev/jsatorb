# JSatorb project: root documentation file

This file is the root documentation file of all the JSatOrb project.
The current JSatOrb module (named 'jsatorb') is the "root" of all the other modules.
It contains information about the following subjects:

- user and developer documentations,
- structure and content of the current "project",
- structure of the JSatOrb development environment (seen in more details in the developer documentation),
- how to prepare the user installation archive.

# User and developer information

All useful documentation for JSatOrb users can be found in [this document](deliverable-resources/README-user.md).  

All useful documentation for JSatOrb developers can be found in [this document](doc/README-dev.md).


# Structure and content of this project

## Raw structure

Below is presented the structure and content of the JSatOrb root project:

<pre>
+- deliverable-resources             'Resources to prepare the user installation archive'
    +- doc                           'Root of all users/developers documentation'
        +- user
            user-install.md          'The user installation guide'
            user-VTS.md              'VTS visualization tool's JSatOrb complementary documentation'
    +- mission-examples              'Mission data sets samples to open in the JSatOrb GUI'
        Mission-Mars.jso
        Mission-Sun-synchronous-orbit.jso
    README-user.md                   'Root documentation for users and developers'
+- doc                               'Root of all developers documentation'
    +- dev
        +- architecture              'JSatOrb architecture document in two formats'
            Architecture_JSatOrb.pdf
            Architecture_JSatOrb.pptx
        dev-install.md               'The developer installation guide'            
        tests.md                     'JSatOrb tests guide'      
    README-dev.md                    'Root documentation for developers'
+- prepare-delivery                  'Tool used to prepare the user installation archive'
    prepare-deliverable.py
+- scripts                           'Scripts gathering folder'
    _runAllTestsCore.sh              'Core script to run all the JSatOrb tests'
    runAllTests.sh                   'Script to run all the JSatOrb tests'
    runTests.sh                      'Script to run all the tests of a JSatOrb module'
README.md                            'The present document'
</pre>

Other resources are also copied and renamed from other git repositories in the process of producing the final user installation archive.
Those deliverable preparation operations are scripted in a bash script which can be [found here](./prepare-delivery/prepare-deliverable.bash).
__Look at the `Preparing the user installation archive` paragraph below on how to use it.__


## JSatOrb development environment structure

The JSatOrb's development environment is detailed in the ___JSatOrb's development environment___ section of the [developer documentation](doc/README-dev.md#jsatorb's-development-environment).

Below is the raw structure of the development environment:

<pre>
+- home
    +- jsatorb                                              'The JSatOrb's dedicated user's home directory'
        +- JSatOrb                                          
            +- repos                                        
                +- git                                      'The JSatOrb modules root folder (git repositories)'
                    +- jsatorb                              'The current JSatOrb module'
                    +- jsatorb-agent                        'JSatOrb agent'
                    +- jsatorb-common                       'JSatOrb common module'
                    +- jsatorb-coverage-service             'Coverage features'
                    +- jsatorb-date-conversion              'Date conversion (JD/MJD) feature'
                    +- jsatorb-docker                       'JSatOrb Docker documentation and scripts'
                    +- jsatorb-eclipse-service              'Eclipse features'
                    +- jsatorb-frontend                     'JSatorb frontend'
                    +- jsatorb-rest-api                     'JSatorb REST API'
                    +- jsatorb-visibility-service           'Visibility and Ephemerides features'
            +- Tools                                        'JSatOrb Tools folder (3rd party, installers and user tools)'
                +- Anaconda3                                'The Anaconda 3 installation directory'
                +- Installers                               'Installation archives'
                +- JSatOrbAgent                             'JSatOrb agent's installation directory (to test as a user)
                +- Vts-Linux-64bits-3.4.2                   'The VTS installation directory (to test as a user)
</pre>

## User documents inner links

User dedicated documents found in deliverable-resources/doc contain relative links to other documents. Those links have to take into account that their final destination is the user archive deliverable.  
So, each document has to assume that its root folder is not **the deliverable-resources folder**, but **one folder deeper**.

On the contrary, the present developers dedicated document uses relative links to child documents according to the real 'jsatorb' module folder layout.

(See already existing links on those two documents categories if the current explanation is not clear).


# Preparing the user installation archive

The user installation archive production is partially automatized thanks to the **[deliverable production bash script](prepare-delivery/prepare-deliverable.bash)**.

Hence, prior to running this script, several non-automatized steps have to be performed manually:

- Building the JSatOrb Agent, [see this documentation to know how](../jsatorb-agent/README.md),
- Exporting the JSatOrb Docker images, [see this documentation to know how](../jsatorb-docker/README.md#extracting_the_jsatorb_docker_images),

Only after performing those manuals steps, can the `prepare-delivery/prepare-deliverable.bash` script be executed.

All the most up-to-date information about the delivery production process is therefore contained in this script file.

The final user installation archive structure and content are presented below:

<pre>
+- JSatOrb                                  'Root of the uncompressed user installation archive'
    +- doc                                  'Root of all user documentation'
        +- images                           'Documentation's illustrations'
        +- user
            user-install.md                 'The user installation guide'
            user-mission-examples.md        'The user installation guide'
            user-VTS.md                     'VTS visualization tool's JSatOrb complementary documentation'
    +- docker_images
        jsatorb-backend-docker-image.tgz
        jsatorb-frontend-docker-image.tgz
        celestrak-json-proxy-docker-image.tgz
    +- JSatOrbAgent
        +- cache                            'JSatOrb agent's cache folder to uncompress received VTS archives'
        +- lib                              'JSatOrbAgent's executable dependencies'
        jsatorb-agent                       'JSatOrbAgent's executable'
    +- mission-data                         'The user/student mission data sets storage folder'
    +- mission-examples                     'Mission data set samples to use in the JSatOrb GUI'
        Mission-Mars.jso
        Mission-Sun-synchronous-orbit.jso
    docker-compose.yml                      'JSatOrb's Docker compose file'
    jsatorb-agent-logs.bash                 'Script to display th JSatOrb Agent logs live'
    jsatorb-backend-logs.bash               'Script to display th JSatOrb backend server logs live'
    jsatorb-load-docker-images.bash         'Script to install the JSatOrb Docker images'
    jsatorb-start.bash                      'Script to start the JSatOrb application'
    jsatorb-stop.bash                       'Script to stop the JSatOrb application'
    README.md(renamed from README-user.md)  'Root documentation for users'
</pre>
