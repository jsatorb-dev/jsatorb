#!/bin/bash

# -----------------------------------------------------------------------------
# JSatOrb project: JSatOrb's user installation archive generation script 
# -----------------------------------------------------------------------------
# This script gathers all needed resources in order to prepare the 
# archive needed by users to install JSatOrb in their Ubuntu 18.04 LTS 64 bit 
# environment. Manual steps are to be performed first.
# See the documentation in the jsatorb/README.md documentation.
# -----------------------------------------------------------------------------
# This script has to be run in a JSatOrb development environment, in the folder
# [JSATORB_GIT_ROOT_FOLDER]/jsatorb/prepare-delivery
# -----------------------------------------------------------------------------

# Current directory
current_dir=`pwd`

# JSatOrb's git repositories root dir
root_dir=$(realpath "$current_dir/../..")

# Target distribution directory
dist_dir="$current_dir/dist"

# JSatOrb Agent's distribution folder
jsatorb_agent_dist_dir="$root_dir/jsatorb-agent/build/exe.linux-x86_64-3.7"


# =============================================================================
# START
# =============================================================================
echo "-- JSatOrb project: JSatOrb's user installation archive generation script"

# Display folders
echo "-- Directories ----------------------------"
echo "current directory is $current_dir"
echo "JSatOrb root directory is $root_dir"
echo "distribution directory is $dist_dir"
echo "JSatOrb Agent's distribution directory is $jsatorb_agent_dist_dir"

# JSatOrb Docker images names
Docker_images_dir="$root_dir/jsatorb-docker/docker_images"
Docker_backend="jsatorb-backend-docker-image.tgz"
Docker_frontend="jsatorb-frontend-docker-image.tgz"
Docker_celestrak="celestrak-json-proxy-docker-image.tgz"


# =============================================================================
# Checking prerequisites:
#  - Generation directory doesn't already exists or is empty,
#  - The JSatOrb's git repositories root dir exists,
#  - The JSatOrb Agent distribution binaries are available,
#  - The JSatOrb Docker images have been exported.
# =============================================================================

echo "-- Checking prerequisites -----------------"
echo "-- Checking existence of the distribution folder"
# Checking the existence of the generation root folder
# If the distribution directory exists
if [ -d "$dist_dir" ]; then
    # If it is not empty
    if [ "$(ls -A $dist_dir)" ]; then
        echo "The target generation directory ($dist_dir) is not empty."
        echo "Please, delete the directory or all of its content and run this script again."
        exit 1
    fi
fi

echo "-- Checking existence of the JSatOrb's git repositories root folder"
# Checking the existence of the JSatOrb's git repositories root folder
if [ ! -d "$root_dir" ]; then
    echo "The JSatOrb's git repositories root folder ($root_dir) does not exists."
    echo "Are you really running this script in a JSatOrb development environment ?"
    echo "Please check and run this script again."
    exit 1
fi

echo "-- Checking the existence of the JSatOrb Agent distribution binaries"
# Checking the existence of the JSatOrb Agent distribution binaries
# If the distribution directory doesn't exists
if [ ! -d "$jsatorb_agent_dist_dir" ]; then
    echo "The JSatOrb Agent distribution folder doesn't exists !"
    echo "Please, follow the procedure given in jsatorb-agent/README.md in order to build the JSatOrb Agent"
    echo "Then, re-run this script."
    exit 1
fi
# If the distribution directory is empty
if [ ! "$(ls -A $jsatorb_agent_dist_dir)" ]; then
    echo "The JSatOrb Agent distribution directory ($jsatorb_agent_dist_dir) is empty !"
    echo "Please, follow the procedure given in jsatorb-agent/README.md in order to build the JSatOrb Agent"
    echo "Then, re-run this script."
    exit 1
fi

echo "-- Checking existence of the JSatOrb Docker images"
# Checking existence of the JSatOrb Docker images
# Backend image
if [ ! -f "$Docker_images_dir/$Docker_backend" ]; then
    echo "The JSatOrb backend Docker image ($Docker_images_dir/$Docker_backend) cannot be found !"
    echo "Please, follow the procedure given in jsatorb-docker/README.md in order to build and export the JSatOrb Docker images"
    echo "Then, re-run this script."
    exit 1
fi
# Frontend image
if [ ! -f "$Docker_images_dir/$Docker_frontend" ]; then
    echo "The JSatOrb frontend Docker image ($Docker_images_dir/$Docker_frontend) cannot be found !"
    echo "Please, follow the procedure given in jsatorb-docker/README.md in order to build and export the JSatOrb Docker images"
    echo "Then, re-run this script."
    exit 1
fi
# Celestrak image
if [ ! -f "$Docker_images_dir/$Docker_celestrak" ]; then
    echo "The JSatOrb Celestrak server Docker image ($Docker_images_dir/$Docker_celestrak) cannot be found !"
    echo "Please, follow the procedure given in jsatorb-docker/README.md in order to build and export the JSatOrb Docker images"
    echo "Then, re-run this script."
    exit 1
fi

# -----------------------------------------------------------------------------
# ISAE: STEP TO CUSTOMIZE
# -----------------------------------------------------------------------------
# To ISAE admins: add a check on the VTS content availability
# -----------------------------------------------------------------------------


# =============================================================================
# Creating the distribution folders
# =============================================================================

echo "-- Preparing the distribution folder ------"

# Create the final user installation archive distribution folders
echo "-- Creating the final user installation archive distribution folders"
mkdir -p "$dist_dir/JSatOrb/mission-data"
mkdir -p "$dist_dir/JSatOrb/doc/user"
mkdir -p "$dist_dir/JSatOrb/docker_images"
mkdir -p "$dist_dir/JSatOrb/JSatOrbAgent/cache"
mkdir -p "$dist_dir/JSatOrb/JSatOrbAgent/lib"

# -----------------------------------------------------------------------------
# ISAE: STEP TO CUSTOMIZE
# -----------------------------------------------------------------------------
# To ISAE admins: VTS folder has to be created
mkdir -p "$dist_dir/JSatOrb/Vts-Linux-64bits-3.4.2"
# -----------------------------------------------------------------------------

# =============================================================================
# Copying the current project's content
# =============================================================================
# This step covers copy of:
#   - user documentation folder
#   - mission examples
#   - README-user.md
# =============================================================================

echo "-- Copying the JSatOrb distribution content: Part 1"

# -----------------------------------------------------------------------------
# Change the working directory to the current project's root folder
# -----------------------------------------------------------------------------
cd ..

# Copy the content of the current project's deliverable resources
cp -r deliverable-resources/* "$dist_dir/JSatOrb"

# Renaming the user root documentation file
mv "$dist_dir/JSatOrb/README-user.md" "$dist_dir/JSatOrb/README.md"


# =============================================================================
# Copying the other project's content
# =============================================================================
echo "-- Copying the JSatOrb distribution content: Part 2"


# Copy the JSatOrb Agent files
cp "$jsatorb_agent_dist_dir/jsatorb-agent" "$dist_dir/JSatOrb/JSatOrbAgent"
cp "$jsatorb_agent_dist_dir/jsatorb-agent.json" "$dist_dir/JSatOrb/JSatOrbAgent"
cp -r "$jsatorb_agent_dist_dir/lib" "$dist_dir/JSatOrb/JSatOrbAgent"

# Copy the Docker images
echo "-- Copying the Docker images"
cp "$root_dir/jsatorb-docker/docker_images/"*.tgz "$dist_dir/JSatOrb/docker_images"

# -----------------------------------------------------------------------------
# ISAE: STEP TO CUSTOMIZE
# -----------------------------------------------------------------------------
# To ISAE admins: VTS decompressed archive structure has to be copied
cp -r ~/JSatOrb/Tools/Vts-Linux-64bits-3.4.2/* "$dist_dir/JSatOrb/Vts-Linux-64bits-3.4.2"
# -----------------------------------------------------------------------------

# Copy the Docker compose file and associated scripts
cp "$root_dir/jsatorb-docker/jsatorb-agent-logs.bash" "$dist_dir/JSatOrb"
cp "$root_dir/jsatorb-docker/jsatorb-backend-logs.bash" "$dist_dir/JSatOrb"
cp "$root_dir/jsatorb-docker/jsatorb-load-docker-images.bash" "$dist_dir/JSatOrb"
cp "$root_dir/jsatorb-docker/jsatorb-start.bash" "$dist_dir/JSatOrb"
cp "$root_dir/jsatorb-docker/jsatorb-stop.bash" "$dist_dir/JSatOrb"
cp "$root_dir/jsatorb-docker/docker-compose.yml" "$dist_dir/JSatOrb"


# =============================================================================
# Generate the JSatOrb's user installation archive
# =============================================================================

echo "-- Generating the user installation archive"

# -----------------------------------------------------------------------------
# Change the working directory to the JSatOrb dist root folder
# -----------------------------------------------------------------------------
cd "$dist_dir"

# Create the archive
tar -zcvf "$dist_dir/jsatorb-user-installation.tar.gz" "JSatOrb"