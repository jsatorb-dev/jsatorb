# JSatOrb project: Development environment installation and setup

These installation notes are dedicated to install a complete JSatOrb development environment in an Xubuntu 18.04 LTS 64 bit environment.

# Configuring the machine

## Install utilities (vim, git, etc...)

Updating apt
```
>sudo apt update
```

Wait for apt to get all updates, this can take a few seconds to a few minutes.

Installing tools:
```
>sudo apt install vim // Text editor
>sudo apt install git // To manage JSatOrb git repositories
>sudo apt install subversion // To manage JSatOrb subversion repositories
>sudo apt install curl // To perform connectivity and HTTP requests tests
>sudo apt install gedit // Text editor
>sudo apt install maven // Maven building tool
>sudo apt install ntp // Network Time Protocol: to synchronize the platform time and get correct commit timestamps
```

Restarting the NTP service:
```
>sudo service ntp restart
```


## Linux's proxy configuration

### Pre-requisites

A 'jsatorb' named user with superuser rights is assumed to be used all along this document:

 - Logged as 'jsatorb' user in the Xubuntu Graphic Desktop,
 - Super-user credentials available.


### Bash proxy config

To be able to install software from the net, declare the following environment variables and put them in a bash script named ~/customize.bash

---

```
#!/bin/bash

# Configure proxy settings
export http_proxy=http://[YOUR_PROXY_URL]:[YOUR_PROXY_PORT]/
export https_proxy=http://[YOUR_PROXY_URL]:[YOUR_PROXY_PORT]/
export ftp_proxy=http://[YOUR_PROXY_URL]:[YOUR_PROXY_PORT]/
export no_proxy="localhost,127.0.0.1,.[YOUR_DOMAIN_SUFFIX]"

export HTTP_PROXY=http://[YOUR_PROXY_URL]:[YOUR_PROXY_PORT]/
export HTTPS_PROXY=http://[YOUR_PROXY_URL]:[YOUR_PROXY_PORT]/
export FTP_PROXY=http://[YOUR_PROXY_URL]:[YOUR_PROXY_PORT]/
export NO_PROXY="localhost,127.0.0.1,.[YOUR_DOMAIN_SUFFIX]"
```

---

Then, edit your ~/.bashrc file in which you will source this file, like this:
```
>vim ~/.bashrc
```

In the .bashrc file, at the bottom:

---
```
# Add specific configuration commands
source ~/customize.bash
```
---

Now, you can effectively source your newly modified .bashrc file with the command:
```
>source ~/.bashrc
```


### APT proxy config

Create file /etc/apt/apt.conf.d/95proxies
```
>sudo touch /etc/apt/apt.conf.d/95proxies
```

Then write inside with editor (such as vim)
```
>sudo vim /etc/apt/apt.conf.d/95proxies
```

---

```
Acquire::http::proxy "http://[YOUR_PROXY_URL]:[YOUR_PROXY_PORT]/";  
Acquire::ftp::proxy "http://[YOUR_PROXY_URL]:[YOUR_PROXY_PORT]/";  
Acquire::https::proxy "http://[YOUR_PROXY_URL]:[YOUR_PROXY_PORT]/";
```

---

__Proxy configuration__ [Source](https://askubuntu.com/questions/150210/how-do-i-set-system-wide-proxy-servers-in-xubuntu-lubuntu-or-ubuntu-studio)


### Firefox proxy configuration

Open the Main Menu > Preferences > General > Network Settings Button [Settings...]

Use the 'Manual proxy configuration'

Set the following settings:
- host = '[YOUR_PROXY_URL]' 
- port '[YOUR_PROXY_PORT]' 
- check the 'Also use this proxy for FTP and HTTPS' checkbox
- In the "No proxy for" text zone, add the value ".[YOUR_DOMAIN_SUFFIX]"

Check that you can browse some website of your choice


### NPM proxy configuration

Run the following two commands in a terminal:
```
>npm config set proxy http://[YOUR_PROXY_URL]:[YOUR_PROXY_PORT]
>npm config set https-proxy http://[YOUR_PROXY_URL]:[YOUR_PROXY_PORT]
```

### Maven proxy configuration

If Maven is not yet installed, see below the process to do so.
To check if Maven is installed (and its version):
```
>mvn -v
```

Go into the Maven folder and create a settings file
```
>cd ~/.m2
>vi settings.xml
```

Add the following content into it:

---
```
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"  
  xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0 http://maven.apache.org/xsd/settings-1.0.0.xsd">

  <!-- localRepository
   | The path to the local repository maven will use to store artifacts.
   |
   | Default: ~/.m2/repository
   | -->
  <localRepository>/home/jsatorb/.m2/repository</localRepository>

  <proxies>
     <proxy>
        <id>proxy-http</id>
        <active>true</active>
        <protocol>http</protocol>
        <host>[YOUR_PROXY_URL]</host>
        <port>[YOUR_PROXY_PORT]</port>
      </proxy>
      <proxy>
        <id>proxy-https</id>
        <active>true</active>
        <protocol>https</protocol>
        <host>[YOUR_PROXY_URL]</host>
        <port>[YOUR_PROXY_PORT]</port>
      </proxy>
   </proxies>

</settings>
```

---

### Configuring git

Set the git proxy configuration
```
>git config --global http.proxy http://[YOUR_PROXY_URL]:[YOUR_PROXY_PORT]
>git config --global user.name "<Your full name: first name and last name>"
>git config --global user.email <firstname>.<lastname>@[YOUR_DOMAIN_SUFFIX]
>git config --global core.editor vim
```

To customize the linux prompt while being in a folder owned by a git repository, also add the following commands in the ~/customize.bash file created previously:

---
```
# Git prompt
parse_git_branch(){
	git branch 2> /dev/null | sed -e'/^[^*]/d' -e 's/* \(.*\)/(\1)/'
}

export PS1="\u@\h\[\033[32m\]\w\[\033[33m\]\$(parse_git_branch)\[\033[00m\]$"
```
---


# Setting up the JSATORB environment development

Follow the folder layout below (which of those folders to create will be indicated step by step) :
```
/home/jsatorb/JSatOrb/ `(main project folder)`
                     /repos/ `(JSatOrb repositories)`
                              /svn/JSatOrbData
                              /git/jsatorb-frontend
                                  /jsatorb-eclipse-service
                                  /jsatorb-visibility-service
                                  /jsatorb-coverage-service
                                  /jsatorb-rest-api
                                  /...
                     /Tools/ `(JSatorb Tools and installers)`
                           /VTS
                           /Anaconda3
                           /Installers/ `(All needed tools and libraries installers)`
                                      /Anaconda3-2018.12-Linux-x86_64.sh
                                      /jdk-8u231-linux-x64.tar.gz
                                      /Vts-Linux-64bits-3.3.1.tar.gz
```

Start by creating the following folders:
```
>mkdir -p /home/jsatorb/JSatOrb/repos/svn
>mkdir -p /home/jsatorb/JSatOrb/repos/git
>mkdir -p /home/jsatorb/JSatOrb/Tools/Installers
```

## Configuring the passwordless access to Odin/TULEAP repositories

### Create public/private key pair

// Go to your jsatorb home directory
```
>cd
```

__Generate a public/private key pair:__
```
>ssh-keygen
Generating public/private rsa key pair.  
Enter file in which to save the key (/home/jsatorb/.ssh/id_rsa): tuleap-git-key-jsatorb  
```

Leave the passphrase empty:
```
Enter passphrase (empty for no passphrase): 
```

Confirm empty passphrase.  
Enter same passphrase again.  
```
Your identification has been saved in tuleap-git-key-jsatorb.
Your public key has been saved in tuleap-git-key-jsatorb.pub.
The key fingerprint is:
SHA256:XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX jsatorb@jsatorb1-xbuntu
The key's randomart image is:
+---[RSA 2048]----+
|XXXXXXXXXXXXXXXXX|
|XXXXXXXXXXXXXXXXX|
|XXXXXXXXXXXXXXXXX|
|XXXXXXXXXXXXXXXXX|
|XXXXXXXXXXXXXXXXX|
|XXXXXXXXXXXXXXXXX|
|XXXXXXXXXXXXXXXXX|
|XXXXXXXXXXXXXXXXX|
|XXXXXXXXXXXXXXXXX|
+----[SHA256]-----+
```

### Configure ODIN/Tuleap

Open a Firefox Web Browser on the machine your are configuring.  
Configure the proxy if not already done (see chapters above).  

Connect to the ODIN/Tuleap Web GUI (https://odin.si.c-s.fr/).  
Go to your account settings (https://odin.si.c-s.fr/account/).  
Click on the "Add keys" button (on the left side).  

Copy the content of the '/home/jsatorb/tuleap-git-key-jsatorb.pub' file into the appearing popup window; content can be obtained with the following command:  
```
>cat /home/jsatorb/tuleap-git-key-jsatorb.pub 
ssh-rsa XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX jsatorb@jsatorbX-xbuntu
```

Copy the content, "ssh-rsa" and "jsatorb@jsatorbX-xbuntu" included.  
Click on the "Save key" button.  

Test Whether ssh-agent is active (PID value may vary):
```
>sudo ssh-agent
SSH_AUTH_SOCK=/tmp/ssh-zeMWmUvAMmxI/agent.7377; export SSH_AUTH_SOCK;
SSH_AGENT_PID=7378; export SSH_AGENT_PID;
echo Agent pid 7378;
```


#### Set the .bashrc

Add your private key in the ssh-agent
```
>eval `ssh-agent -s`
Agent pid 7716

>ssh-add

>ssh-add ~/tuleap-git-key-jsatorb
Identity added: /home/jsatorb/tuleap-git-key-jsatorb (/home/jsatorb/tuleap-git-key-jsatorb)
```

Also add these three commands at the end of the ~/customize.bash file created previously:

---

```
# Tuleap ssh key config
eval `ssh-agent -s`
ssh-add
ssh-add ~/tuleap-git-key-jsatorb
```

---


## Clone the JSatOrb git repositories

To do so, go in the JSatOrb root folder (__/home/jsatorb/JSatOrb/repos/git__),  
and run the following commands:
```
>git clone ssh://gitolite@odin.si.c-s.fr/jsatorb/jsatorb.git
>git clone ssh://gitolite@odin.si.c-s.fr/jsatorb/jsatorb-agent.git
>git clone ssh://gitolite@odin.si.c-s.fr/jsatorb/jsatorb-common.git
>git clone ssh://gitolite@odin.si.c-s.fr/jsatorb/jsatorb-coverage-service.git
>git clone ssh://gitolite@odin.si.c-s.fr/jsatorb/jsatorb-date-conversion.git
>git clone ssh://gitolite@odin.si.c-s.fr/jsatorb/jsatorb-docker.git
>git clone ssh://gitolite@odin.si.c-s.fr/jsatorb/jsatorb-eclipse-service.git
>git clone ssh://gitolite@odin.si.c-s.fr/jsatorb/jsatorb-frontend.git
>git clone ssh://gitolite@odin.si.c-s.fr/jsatorb/jsatorb-rest-api.git
>git clone ssh://gitolite@odin.si.c-s.fr/jsatorb/jsatorb-visibility-service.git
```


## Add Ubuntu 18.04 LTS repository and needed libraries

Add a repository to the apt install tool:
```
>sudo add-apt-repository "deb http://mirrors.kernel.org/ubuntu/ xenial main"
```
Wait a few seconds for the repository to be installed

For VTS
```
>sudo apt install libjpeg62
>sudo apt install libpng12-0
```

For PrestoPlot (the first one is 150 MB, so it can take a few seconds to perform)
```
>sudo apt-get install gcc-multilib
>sudo apt-get install libx11-6:i386
```


## Installing NodeJS

Verify current installed version:
```
>node -v
```

If no Node.js version is installed, the message will be: "Command 'node' not found, but can be installed with: sudo apt install nodejs", proceed directly to __STEP 2__ below.

If a previous Node.js is installed by default with Xubuntu 18.04 (v8.10.0), uninstall it like this:
 __STEP 1:__
```
>sudo apt remove nodejs
>sudo apt purge nodejs
>sudo apt autoremove
```

__STEP 2:__
Add the NodeJS PPA to your system using the following commands:  

1. Add the 'software-properties-common' package.
```console
>sudo apt install software-properties-common
```

2. Next, add the NodeJS PPA (A deprecation warning is shown during 20 seconds, wait for it to end).
```
>sudo curl -sL https://deb.nodesource.com/setup_11.x | sudo -E bash -
```
(if curl is not installed, 'sudo apt install curl')

A deprecation warning will be displayed, saying that Node.js 11.x is no longer actively supported.  
Wait for the 20 seconds and let the script run through its end.

At this step, several problems can occur:
- if the curl proxy is not set, use the -x option to set the proxy, as following:
```
>sudo curl -x proxy.url:port -sL [...] | sudo -E bash -
```
- if the installation still fails, download the NodeJS setup script with the following commands:
```
>sudo curl -x proxy.url:port -sL https://deb.nodesource.com/setup_11.x > ./setup_11.x
>chmod u+x ./setup_11.x
>setup_11.x
```

__Once the step above succeeds, continue the installation.__

Install the Node.js v11:
```
>sudo apt-get install -y nodejs
```

Verify current installed version:
```
>node -v
v11.15.0
```

__Install/uninstall procedure__ [Source: https://www.journaldev.com/27373/install-uninstall-nodejs-ubuntu](https://www.journaldev.com/27373/install-uninstall-nodejs-ubuntu)


## Installing Angular

Install angular v8.3.20 as global:
```
>npm install -g @angular/cli@8.3.20
npm WARN deprecated request@2.88.2: request has been deprecated, see https://github.com/request/request/issues/3142
/usr/bin/ng -> /usr/lib/node_modules/@angular/cli/bin/ng

@angular/cli@8.3.20 postinstall /usr/lib/node_modules/@angular/cli
node ./bin/postinstall/script.js

? Would you like to share anonymous usage data with the Angular Team at Google under
Google’s Privacy Policy at https://policies.google.com/privacy? For more details and
how to change this setting, see http://angular.io/analytics. No
+ @angular/cli@8.3.20
added 260 packages from 205 contributors in 44.649s
```

In the jsatorb-frontend folder
```
>cd ~/JSatOrb/repos/git/jsatorb-frontend/
>npm install
```
There is again a question about sharing anonymous data ..., answer No and continue.  
There is multiple warnings at the end of the installation process.

Install the Angular Material Extra Components (DateTimePicker, TimePicker, ColorPicker).
To do so, [see the jsatorb-frontend project's documentation here](../../../jsatorb-frontend/README.md#prerequisites).


Project's webpage [source](https://github.com/h2qutc/angular-material-components)

To start the server:
```
>ng serve
```
A message tells that the global Angular CLI version is greater than the local one.  
Then, the server is up and running on localhost:4200.


## Installing Anaconda3

Go into the /home/jsatorb/JSatOrb/Tools/Installers folder:
```
>cd ~/JSatOrb/Tools/Installers
```

Download the Anaconda installation script
```
>wget https://repo.anaconda.com/archive/Anaconda3-2018.12-Linux-x86_64.sh
```

Set the script as executable:
```
>chmod +x ./Anaconda3-2018.12-Linux-x86_64.sh
```

Run the Anaconda3 installation script:
```
>./Anaconda3-2018.12-Linux-x86_64.sh

[...]
<	Licence terms       >
[...]

Do you accept the license terms? [yes|no]
[no] >>> yes

Anaconda3 will now be installed into this location:
/home/jsatorb/anaconda3

  - Press ENTER to confirm the location
  - Press CTRL-C to abort the installation
  - Or specify a different location below

[/home/jsatorb/anaconda3] >>> /home/jsatorb/JSatOrb/Tools/Anaconda3
PREFIX=/home/jsatorb/JSatOrb/Tools/Anaconda3
installing: python-3.7.1-h0371630_7 ...
Python 3.7.1
installing: blas-1.0-mkl ...
installing: ca-certificates-2018.03.07-0 ...
installing: conda-env-2.6.0-1 ...
[...]
installing: anaconda-2018.12-py37_0 ...
installing: conda-4.5.12-py37_0 ...
installing: conda-build-3.17.6-py37_0 ...
installation finished.
Do you wish the installer to initialize Anaconda3
in your /home/jsatorb/.bashrc ? [yes|no]
[no] >>> yes

Initializing Anaconda3 in /home/jsatorb/.bashrc
A backup will be made to: /home/jsatorb/.bashrc-anaconda3.bak


For this change to become active, you have to open a new terminal.

Thank you for installing Anaconda3!

===========================================================================

Anaconda is partnered with Microsoft! Microsoft VSCode is a streamlined
code editor with support for development operations like debugging, task
running and version control.

To install Visual Studio Code, you will need:
  - Administrator Privileges
  - Internet connectivity

Visual Studio Code License: https://code.visualstudio.com/license

Do you wish to proceed with the installation of Microsoft VSCode? [yes|no]
>>>yes
Proceeding with installation of Microsoft VSCode
Checking Internet connectivity ...
Installing Visual Studio Code ...
[sudo] password for jsatorb:
OK
Hit:1 http://fr.archive.ubuntu.com/ubuntu bionic InRelease
Get:2 http://security.ubuntu.com/ubuntu bionic-security InRelease [88,7 kB]                                        
[...]
libx11-xcb1 is already the newest version (2:1.6.4-3ubuntu0.2).
libx11-xcb1 set to manually installed.
Some packages could not be installed. This may mean that you have
requested an impossible situation or if you are using the unstable
distribution that some required packages have not yet been created
or been moved out of Incoming.
The following information may help to resolve the situation:

The following packages have unmet dependencies:
 libgtk2.0-0-dbg : Depends: libgtk2.0-0 (= 2.24.30-1ubuntu1) but 2.24.32-1ubuntu1 is to be installed
E: Unable to correct problems, you have held broken packages.
Updating VSCode Config ...
Installing Extensions ...
Installing extensions...
Installing extension 'ms-python.anaconda-extension-pack' v1.0.1...
Extension 'ms-python.anaconda-extension-pack' v1.0.1 was successfully installed.
VSCode successfully installed in /usr/share/code !
```

The Anaconda 3 installation script execution is now over.

Source the newly modified .bashrc file:
```
>source ~/.bashrc
```

Add the conda-forge channel to the list of known channels:
```
>conda config --add channels conda-forge
```

Create a dedicated JSatOrb environment with a defined Python version (this prevents unwanted Python updates which cripple the Anaconda3 installation !)
```
>conda create -n JSatOrbEnv python=3.7 orekit jinja2
Proceed ([y]/n)? y

Preparing transaction: done
Verifying transaction: done
Executing transaction: done
```

Activate this environment:
```
>conda activate JSatOrbEnv (_to deactivate, the command is: >conda deactivate_).
```

Install bottle in a second time (otherwise an error occurs when trying to install it at the same time we create the environment...)
```
>conda install bottle
Proceed ([y]/n)? y

Preparing transaction: done
Verifying transaction: done
Executing transaction: done
```

### Anaconda3 additional steps for Orekit

In case the official Orekit v10.2 release is not available at the time you are installing the JSatOrb environment, you'll have to follow the additional installation steps below:

1. Activate the JSatOrb environment if it's not already the case:
```
>conda activate JSatOrbEnv
```

2. Uninstall orekit previous version:
```
>conda remove orekit
```

3. Get the Orekit v10.2 SNAPSHOT version from the CS Group ODIN server (if you need access to this file, please contact Thibault Gateau):

The file is in the JSatOrb Tuleap Forge, in the SVN repository named __JSatOrbData__, in the __Installeurs__ folder and is named:  
__orekit-10.2-py37he1b5a44_0.tar.bz2__.

4. Install the Orekit v10.2 SNAPSHOT conda package:
```
>conda install ./orekit-10.2-py37he1b5a44_0.tar.bz2
```


### Useful links

Orekit conda-forge URL: [Source](https://anaconda.org/conda-forge/orekit)
Bottle conda-forge URL: [Source](https://anaconda.org/conda-forge/bottle)


## Installing Java

Instead of the procedure given in the _Source_ link (coming from the Supaero installation process, see below), we install the OpenJDK 8 directly with _apt_:
```
>sudo apt install openjdk-8-jdk
```

Check the java installation
```
>java -version
```
Should be 1.8.0_152-release-1056-b12
```
>javac -version
```
Should be 1.8.0_152-release

[Source](https://doc.ubuntu-fr.org/java_proprietaire)


### Additional installation information

Each JSatOrb module can list additional specific dependencies which needs to be installed prior to be able to launch the module.  
Those informations are generally in the 'Prerequisite' section of the documentation.
The list below is exhaustive and will need an update if a module which don't have specific dependency for now becomes to have one in the future.

List of modules needing a specific dependency complementary installation:
- jsatorb-common: details are given in [the root README.md of the module](../../../jsatorb-common/README.md).
- jsatorb-coverage-service: details are given in [the root README.md of the module](../../../jsatorb-coverage-service/README.md).
- jsatorb-frontend: details are given in [the root README.md of the module](../../../jsatorb-frontend/README.md).

Then, in order to be able to use Docker to generate the user installation archive, the steps described [in this documentation](../../../jsatorb-docker/README.md) has to be followed.


## Test installation

### Angular front-end

In a terminal, as user _jsatorb_, run the Angular server:
```
>cd ~/JSatOrb/repos/git/jsatorb-frontend
>ng serve
Your global Angular CLI version (9.0.6) is greater than your local
version (8.1.3). The local Angular CLI version is used.
[...]
** Angular Live Development Server is listening on localhost:4200, open your browser on http://localhost:4200/ **
ℹ ｢wdm｣: Compiled successfully.
```

You can now open a Web Browser an connect to _http://localhost:4200_


### Python REST API (backend)

In a terminal, as user _jsatorb_, run the Python server:
```
>cd ~/JSatOrb/repos/git/jsatorb-rest-api
>conda activate JSatOrbEnv
>python src/JSatOrbREST.py
Bottle v0.12.17 server starting up (using WSGIRefServer())...
Listening on http://127.0.0.1:8000/
Hit Ctrl-C to quit.
```

You can now run mission analysis in the JSatOrb web GUI.


## Installing VTS

Copy the VTS archive from the SVN repository to the Tools folder:
```
>cp ~JSatOrb/repos/svn/JSatOrbData/Installeurs/Vts-Linux-64bits-3.4.2.tar.gz ~/JSatOrb/Tools
```

Uncompress the archive:
```
>cd ~/JSatOrb/Tools
>tar -zxvf ./Vts-Linux-64bits-3.4.2.tar.gz
```

Move the installer into the 'Installers' folder:
```
>mv ./Vts-Linux-64bits-3.4.2.tar.gz Installers/
```


### Additional apt packages needed

This information is probably (and should be) in the VTS documentation, but it has to be noted that installing VTS (3.4.2 at least) in an Ubuntu 18.04 LTS environment requires the following additional apt packages:

For VTS itself:
- lib JPEG 62
- lib png 12-0

For PrestoPlot:
- gcc multilib
- lib X11-6 i386

Here are the commands to update the environment:

First, add a new apt repository:
```
>sudo add-apt-repository "deb http://mirrors.kernel.org/ubuntu/ xenial main"
```
Wait a little bit, as it can take a few seconds to update.

Then winstall the needed packages:
```
>sudo apt update
>sudo apt install libjpeg62
>sudo apt install libpng12-0
>sudo apt install gcc-multilib
>sudo apt install libx11-6:i386
```


### Test run

Go into the VTS folder:
```
>cd ~/JSatOrb/Tools/Vts-Linux-64bits-3.4.2
>./startVTS
```

Open a project of your choice.  
Add the PrestoPlot application, run the project/Broker and check if PrestoPlot is running correctly.
