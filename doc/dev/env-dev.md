# JSatorb project: The development environment


# Development environment structure

When the JSatOrb's development environment has been completely installed, following the [installation procedure](dev-install.md), it should look like the structure presented below:

<pre>
+- home
    +- jsatorb                                              'The JSatOrb's dedicated user's home directory'
        +- JSatOrb                                          'The JSatOrb's root folder'
            +- repos                                        'Root of JSatOrb repositories'
                +- git                                      'The JSatOrb modules root folder (git repositories)'
                    +- jsatorb                              'The current JSatOrb module'
                    +- jsatorb-agent                        'The JSatOrb agent's {source code, deployment procedure}'
                    +- jsatorb-docker                       'The JSatOrb servers dockerisation module'
                    +- jsatorb-common                       'The JSatOrb common module {common JSatOrb libraries source code: AEM, MEM,'  
                                                            'file conversion, mission data management, constellation, VTS}'
                    +- jsatorb-coverage-service             'The Coverage features {Coverage features source code, called from REST API}'
                    +- jsatorb-date-conversion              'The date conversion (JD/MJD) feature {Date conversion feature, called from REST API}'
                    +- jsatorb-eclipse-service              'The Eclipse features {Eclipse features source code, called from REST API}'
                    +- jsatorb-frontend                     'The JSatorb frontend {Runnable and source code}'
                    +- jsatorb-rest-api                     'The JSatorb REST API {Runnable and source code}'
            +- Tools                                        'JSatOrb Tools folder (Third party libraries, installers and user tools)'
                +- Anaconda3                                'The Anaconda 3 installation directory'
                +- Installers                               'Installation archives'
                    +- Anaconda3-2018.12-Linux-x86_64.sh    'Anaconda 3 installer'
                    +- cx_Freeze-master-v6.1-source.zip     'CxFreeze installer (used to build the JSatOrb agent's executable)'
                    +- Vts-Linux-64bits-3.4.2.tar.gz        'The VTS installation archive'
                +- JSatOrbAgent                             'The JSatOrb agent's installation directory (to test JSatOrb as a user)
                +- Vts-Linux-64bits-3.4.2                   'The VTS installation directory (to test JSatOrb as a user)
</pre>


# IDE

The IDE used to develop JSatOrb is VSCode. The tool is installed at the same time as Anaconda 3 which includes it in its installation process.
Several extensions are then added afterward to VSCode to increase its capabilities, [as described here](dev-install.md#extensions).


# Technologies

## Frontend

The JSatOrb frontend is developed using:

- Dependencies manager: npm 6.7
- Languages: NodeJS 11.15 / Typescript 3 / Angular 8
- Main dependencies:
    - Material extensions:
        - datetime-picker 2.0.4
        - moment-adapter 2.0.2
    - Image cropper 3.1.8
    - Nanospace client lib 0.4.2
    - Bootstrap 5.1.0

__The complete and most up-to-date dependencies list can be obtained by running the following command in the JSatOrb's development environment, in the JSatorb's frontend module directory (~/JSatOrb/repos/git/jsatorb-frontend):__
```
>npm ls
```

__The web browser on which the JSatOrb frontend server has been validated and therefore has been verified to run on is Firefox 77.0.1 64 bits.__
(For instance, the JSatOrb VTS binary download mechanism doesn't work correctly on Firefox 59).



## Backend

The JSatOrb backend modules are developed using:

- Dependencies managers: Anaconda 3, pip 20.0.2,
- Language: Python 3.7,
- Main dependencies:
    - bottle 0.12.17,
    - jinja2 2.11.1,
    - matplotlib 3.2.1,
    - numPy 1.18.1,
    - Orekit 10.2-SNAPSHOT python wrapper (Java library wrapped thanks to JCC).

A virtual Python environment (named 'JSatOrbEnv'), gathering all the needed dependencies, is used to run the whole backend.

__The complete and most up-to-date dependencies list can be obtained by running the following commands in the JSatOrb's development environment, in one of the JSatorb's backend modules directory (~/JSatOrb/repos/git/jsatorb-rest-api for instance):__

```
>conda activate JSatOrbEnv
>conda list
```


## JSatOrb Agent

The JSatOrb Agent is developed in Python 3.7 and built as a standalone executable using CxFreeze 6.0, in order to avoid to install Python in the user environment.

A virtual Python environment (named __JSatOrbAgentEnv3.7__), gathering all the needed dependencies, is used to build the JSatOrb Agent, as described in the __README.md__ file of the __jsatorb-agent__ module.  
All the details on the way to build and deploy the JSatOrb Agent are also given in this documentation.

__The complete and most up-to-date dependencies list can be obtained by running the following commands in the JSatOrb's development environment, in the JSatorb Agent module directory (~/JSatOrb/repos/git/jsatorb-agent):__

```
>conda activate JSatOrbEnv
>conda list
```