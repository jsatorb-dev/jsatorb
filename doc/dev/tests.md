# JSatorb project: Running the tests

The JSatOrb tests can be run only in the JSatOrb development environment, being logged as the 'jsatorb' user.

#### Run module tests

To run the tests of a single JSatOrb module, the jsatorb user has to :

- change to the chosen __JSatOrb module root folder__ (e.g. 'jsatorb-common'), 
- then go in the __test__ folder,
- and finally run the __runTests.sh__ script.

The tests output logs are only written in the console, but can be written, additionnaly to the console output, in a file of your choice with the Linux __tee__ command.  

```
Example: >./runTests.sh | tee ./[YOUR_CHOSEN_FILENAME]
```

#### Run all tests

To run the whole JSatOrb tests, the jsatorb user has to :

- change to the __'jsatorb' project root folder__, 
- then go in the __scripts__ folder,
- and finally run the __runAllTests.sh__ script.

The whole tests output logs are written in a runAllTests_[TIMESTAMP].log file directly in the scripts folder.

___Remark__: _This script, which runs the whole JSatOrb tests, is simply constituted of a sequence of shell commands running each module's individual __runTests.sh__ scripts and gathering all the output logs into a single file._
