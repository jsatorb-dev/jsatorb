# JSatOrb project: Developer installation procedure

# Development environment

The OS used to develop JSatOrb is Ubuntu v18.04 LTS.  
The IDE used to develop JSatOrb is Visual Studio Code (hereafter called VSCode) v1.43.0 (see detailed ___Help>About___ informations below).

```
        Version: 1.43.0
        Commit: 78a4c91400152c0f27ba4d363eb56d2835f9903a
        Date: 2020-03-09T19:44:52.965Z
        Electron: 7.1.11
        Chrome: 78.0.3904.130
        Node.js: 12.8.1
        V8: 7.8.279.23-electron.0
        OS: Linux x64 4.15.0-91-generic
```

## Installation procedure

### Pre-requisites

- JSatOrb development environment is designed to be installed in a Linux Ubuntu 18.04 LTS 64 bit environment.

### Procedure

The developer complete and detailed procedure to install a JSatOrb development environment is described in [this file](./dev-detailed-install-procedure.md) (a separate file from this one to ease maintenance).


## Extensions

VSCode provides a lot of potential features by offering a lot of extensions.
For the JSatOrb Development, the following extensions are used:

- **Rest Client**: REST Client for Visual Studio Code (v0.23.2 - Author: Huachao Mao): This extension enables to send HTTP requests to the JSatOrb REST API without using the JSatOrb GUI. It is a real helper tool in order to develop, but also to perform software tests and validation. All the *.http files found in the JSatOrb sources are scripts (very readable and similar to JSON format) used to send requests to the JSatOrb REST API. The response is also received in the VSCode IDE and can be scrutated to check result correctness.
- **PlantUML**: Rich PlantUML support for Visual Studio Code (v2.13.11 - Author: jebbs): This extension enables to add scripted therefore easy to maintain UML diagrams into the markdown development documentation files. There is three different possible configurations each with pros and cons:
    1. **Configuration 1: using local preview**:
        - **Description:** With this configuration, one cannot directly display UML diagrams within mardown files. Cursor has to be placed into the PlantUML script, and with the [Alt-D] shortcut, the diagram is displayed in a second pane, on the right of the markdown file. 
        - **Pros:** No installation, confidentiality (diagrams stay local), no web connection needed.  
        - **Cons:** No embedded visualization in the markdown files, performance is far from optimal.  
    2. **Configuration 2: using the public PlantUML server**
        - **Description:** With this configuration, it is now possible to directly display UML diagrams within mardown files preview (using the <Ctrl-Shift-V shortcut)>).
        - **Pros:** Embedded visualization in the markdown files preview, no installation.  
        - **Cons:** No confidentiality (plantUML scripts are rendered into diagrams by a distant public server) and need a web connection.
        Open this extension's settings (Extension tab, then click on the gear icon then _extension settings_) and configurate the following key/values:
            - Plantuml: Render = PlantUMLServer
            - Plantuml: Server = https://www.plantuml.com/plantuml
    3. **Configuration 2: using a local PlantUML server**
        - **Description:** With this configuration, one cannot directly display UML diagrams within mardown files. Cursor has to be placed into the PlantUML script, and with the <Alt-D> shortcut, the diagram is displayed in a second pane, on the right of the markdown file. 
        - **Pros:** Embedded visualization in the markdown files preview, confidentiality (diagrams stay local).  
        - **Cons:** Needs to install a PlantUML server (never tried), see the PlantUML documentation [on the official website](https://www.plantuml.com/plantuml) to do so.  
        Open this extension's settings (Extension tab, then click on the gear icon then _extension settings_) and configurate the following key/values:
            - Plantuml: Render = PlantUMLServer
            - Plantuml: Server = [YOUR_PLANTUML_SERVER_URL]

# Tests

## Functional tests

All sub-modules tests are gathered in the **test** folder of each module or sub-module.
Each module's README.md document describes how to run the module's tests.

## REST API tests

The REST API can be tested thanks to the __*.http files__ of the __test-rest folder__ content. __The REST Client plugin has to be installed in VS Code first.__


## Install JSatOrb in the user environment

In order to run JSatOrb in the user environment, prerequisites are to be met:
- JSatOrb is designed to be installed in a Linux Ubuntu 18.04 LTS 64 bit environment.
- If the user machine acces the internet through a corporate proxy, the common Linux tools have to be configured accordingly (apt, firefox, etc...).
- Docker 19.03+ has to be available in the user environment.
- Docker compose 3.8+ has to be available in the user environment.
- The VTS/Prestoplot additional libraries have been installed by ISAE Administrators (see the "VTS additional libraries" paragraph below),
- The JSatorb installation archive has been provided to the user (__jsatorb-user-installation.tar.gz)__.

The procedure to install and configure Docker and Docker-compose is given [in this documentation](../../../jsatorb-docker/README-install.md).

Now that the prequisites are met in the user environment, the [user installation procedure](../../deliverable-resources/doc/user/user-install.md) has to be performed.
