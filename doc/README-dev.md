# JSatorb project: developers root documentation file

This README is the root of the whole JSatOrb documentation dedicated to developers.
It gives directions about every subject useful to install, test, develop and understand JSatOrb's structure and operation.

__Remark:__ __Additional documentation exists for each JSatOrb module, in the JSatOrb repositories. Those documents are therefore primarily accessible to developers only, but some of them could possibly be useful to users. They can be made accessible to users through a Git repository manager you would have set up (like GitLab, GitHub, BitBucket, ...) or by extracting them from the different repositories and packaged. Those documents are not packaged in the user installation archive to avoid duplication.__

# Installation

The developer's installation procedure is available [in this document](dev/dev-install.md).


# JSatOrb's development environment

The JSatOrb development environment's structure is described in [this document](dev/env-dev.md).


# JSatorb's architecture

The JSatOrb original architecture document is available in Microsoft Powerpoint format. It has been exported in PDF format to ease its visualization in a Linux environment.  

The Powerpoint format document is available following [this link](dev/architecture/Architecture_JSatOrb.pptx).  
The PDF format document is available following [this link](dev/architecture/Architecture_JSatOrb.pdf).  
___Remark:___ _VSCode extensions are available to display PDF format files._


# JSatOrb tests

The documentation to run the JSatOrb tests is [available here](dev/tests.md).


# Mission examples

The two mission data set examples discussed [in the user documentation](../deliverable-resources/README-user.md#mission_examples) can be generated in a JSatOrb development environment using the following procedure:

- running a JSatOrb REST API,
- in a VSCode with the REST client extension installed, opening the **test-rest/MissionDataManager.http** file of the **jsatorb-rest-api** module and running the two requests in the __MISSION EXAMPLES: SUN-SYNCHRONOUS AND MARS MISSION DATA SET SAVE REQUESTS__ section and which header labels are:
    - __Send a request for a Sun-synchronous orbit__
    - __Send a request for a Martian orbit__
