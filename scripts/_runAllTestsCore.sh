#!/bin/bash

# -----------------------------------------------------------------------------
# JSatOrb Project: JSatOrb main module
# -----------------------------------------------------------------------------
# JSatorb Tests running global core script
# -----------------------------------------------------------------------------
# Arguments:
#  No Argument
# -----------------------------------------------------------------------------
# Exit codes:
#   0: No problem occured.
#   1: The user aborted the tests launch.
#   2: Orekit data archive is unavailable (but necessary for the tests).
# -----------------------------------------------------------------------------
# This script assumes it is executed from the the scripts subfolder of the
# 'jsatorb' module.
# It also assumes that the other JSatOrb modules are present at the same folder
# depth, siblings of the 'jsatorb' module, according to the regular JSatOrb 
# Development Environment layout.
# -----------------------------------------------------------------------------

# -- LIST OF MODULES TO TEST --
# This list have to be updated whenever a new module contains tests to be run
# automatically within this script.
modulesToTest="jsatorb-common \
               jsatorb-coverage-service \
               jsatorb-date-conversion \
               jsatorb-eclipse-service"
# -- LIST OF MODULES TO TEST --


curDir=$PWD
moduleGenericTestsScript="runTests.sh"

LIGHT_GREEN="\e[92m"
LIGHT_RED="\e[91m"
LIGHT_BLUE="\e[94m"
YELLOW="\e[33m"
RESET_COLORS="\e[0m"

# Function that reset output text colors
function resetColors()
{
	\echo -e "$RESET_COLORS"
}

# Function that traps the Program interrupt and Program terminate signals
function trapSignals()
{
    echo -e "$YELLOW"
    echo "The program termination has been asked. Exiting"
    resetColors
    exit 2
}

# Activate the trap
trap trapSignals SIGINT SIGTERM

# ---------------------------------------------------------
# This function converts a duration in seconds into Hours, minutes and seconds.
# ---------------------------------------------------------
# Arguments:
#   duration: a duration in seconds.
# ---------------------------------------------------------
function secondsToHMS () {
    num=$1
    min=0
    hour=0
    day=0
    if((num>59));then
        ((sec=num%60))
        ((num=num/60))
        if((num>59));then
            ((min=num%60))
            ((num=num/60))
            if((num>23));then
                ((hour=num%24))
                ((day=num/24))
            else
                ((hour=num))
            fi
        else
            ((min=num))
        fi
    else
        ((sec=num))
    fi
    echo "$day day(s) $hour hour(s) $min min $sec s"
}

# ---------------------------------------------------------
# Function that runs all the tests of a JSatOrb module.
# ---------------------------------------------------------
# It checks that the runTests.sh script is available in the 
# module folder.
# If it's not the case, it copies the 'jsatorb' generic
# script in the module before executing it.
# ---------------------------------------------------------
# Arguments:
#   module: the module to run the tests of.
# ---------------------------------------------------------
function runModuleTests()
{
    module=$1
    moduleDir="$curDir/../../$module"
    # Check if the module exists
    if [ -d "$moduleDir" ]; then
        moduleScript="$moduleDir/$moduleGenericTestsScript"
        # Check if the tests script exists in the module folder
        if [ ! -f "$moduleScript" ]; then
            echo -e "$LIGHT_BLUE"
            echo "The script to run the tests has not been found in module $module (file $moduleScript doesn't exists)."
            echo "Copying the generic one from the 'jsatorb' module."
            resetColors
            cp "./$moduleGenericTestsScript" "$moduleDir"
        fi

        # Run the tests script in automatic mode
        cd "$moduleDir"
        ./runTests.sh -a 2>&1
        cd -
    else
        echo -e "$YELLOW"
        echo "The module $module has not been found (folder $moduleDir doesn't exists) !"
        resetColors
    fi
}

# ---------------------------------------------------------
# The script main entry point
# ---------------------------------------------------------
# It defines the list and order of JSatOrb module tests
# to run.
# ---------------------------------------------------------

# Get the starting date/time
start=`date +%s`
startingDate=`date --utc +%FT%TZ`

echo -e "$LIGHT_GREEN"
echo "-------------------------------------------------------------------------------"
echo "Running all JSatOrb tests: START"
echo "-------------------------------------------------------------------------------"
echo
echo -e "$LIGHT_BLUE"
echo "List of modules to test:"
for module in $modulesToTest
do
    echo "    -$module"
done
resetColors

# Run the modules tests
for module in $modulesToTest
do
    runModuleTests $module
done

# Get the ending date/time
end=`date +%s`

delta=$(( $end - $start ))
duration=`secondsToHMS "$delta"`

# Output the tests scripts running duration
echo -e "$LIGHT_BLUE"
echo "The JSatOrb's global tests running script started at $startingDate" 
echo "Script running duration: $duration"
echo -e "$LIGHT_GREEN"
echo "-------------------------------------------------------------------------------"
echo "Running all JSatOrb tests: END"
echo "-------------------------------------------------------------------------------"
resetColors
