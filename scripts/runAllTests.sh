#!/bin/bash

# -----------------------------------------------------------------------------
# JSatOrb Project: JSatOrb main module
# -----------------------------------------------------------------------------
# JSatorb Tests running global script
# -----------------------------------------------------------------------------
# Arguments:
#  No Argument
# -----------------------------------------------------------------------------
# This script runs the JSatOrb Core global tests run script and 
# capture its output to a log file.
# -----------------------------------------------------------------------------

# Get the timestamp
timestamp=`date +%F_%H-%M-%S`

./_runAllTestsCore.sh | tee -a "runAllTests_$timestamp.log"

